<?php

namespace App\Http\Controllers;

use App\Models\WebController;
use Illuminate\Http\Request;

class WebControllerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = WebController::all();
        return view('page-produk', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page-jual');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'gambar' => 'mimes:jpeg,png,jpg,gif,svg',
        ]);
        

        // headspace-12313.jpg

        $gmbrName = $request->gambar->getClientOriginalName() . '_' . time() 
                                . '.' . $request->gambar->extension();
        $request->gambar->move(public_path('gambar'), $gmbrName);

        WebController::create([
            'nama' => $request->nama,
            'kategori' => $request->kategori,
            'stok' => $request->stok,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
            'gambar' => $gmbrName
        ]);

        return redirect('produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebController  $webController
     * @return \Illuminate\Http\Response
     */
    public function show(WebController $id)
    {
        $detail = WebController::find($id);
        return view('page-detail', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebController  $webController
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = WebController::findorfail($id);
        return view('page-edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebController  $webController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = WebController::findorfail($id);
        $edit->update($request->all());

        return redirect('produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebController  $webController
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebController $webController, $id)
    {
        $hapus = WebController::findorfail($id);
        $hapus->delete();

        return back();
    }
}

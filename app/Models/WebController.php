<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebController extends Model
{
    use HasFactory;
    protected $fillable = [
        'id', 'nama', 'kategori', 'stok', 'deskripsi', 'harga', 'gambar'
    ];
}

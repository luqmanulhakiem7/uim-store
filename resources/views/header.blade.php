	<!-- Start Header Area -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="/"><img src="img/logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item active"><a class="nav-link" href="/">Home</a></li>
							<li class="nav-item active"><a class="nav-link" href="/produk">Produk</a></li>
							{{-- <li class="nav-item submenu dropdown"> --}}
								{{-- <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Customer</a>
								<ul class="dropdown-menu"> --}}
									{{-- <li class="nav-item"><a class="nav-link" href="category.html">Produk</a></li> --}}
									{{-- <li class="nav-item"><a class="nav-link" href="single-product.html">Product Details</a></li> --}}
									{{-- <li class="nav-item"><a class="nav-link" href="checkout.html">Checkout</a></li> --}}
									{{-- <li class="nav-item"><a class="nav-link" href="cart.html">Keranjang</a></li> --}}
									{{-- <li class="nav-item"><a class="nav-link" href="confirmation.html">Konfirmasi Pembayaran</a></li> --}}
								{{-- </ul> --}}
							{{-- </li> --}}
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Pedagang</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="/jual-produk">Jual Barang</a></li>
									{{-- <li class="nav-item"><a class="nav-link" href="single-blog.html">Blog Details</a></li> --}}
								</ul>
							</li>
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Admin</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="/dashboard">Dashboard</a></li>
									{{-- <li class="nav-item"><a class="nav-link" href="tracking.html">Tracking</a></li> --}}
									{{-- <li class="nav-item"><a class="nav-link" href="elements.html">Elements</a></li> --}}
								</ul>
							</li>
							{{-- <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li> --}}
						</ul>
						<ul class="nav navbar-nav navbar-right">
							{{-- <li class="nav-item"><a href="#" class="cart"><span class="ti-bag"></span></a></li> --}}
							<li class="nav-item">
								<button class="search"><span class="lnr lnr-magnifier" id="search"></span></button>
							</li>
                            <li>
                                @if (Route::has('login'))
                                    <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                                        @auth
                                            {{-- <a href="{{ url('/') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a> --}}
                                        @else
                                            {{-- <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a> --}}

                                            @if (Route::has('register'))
                                                {{-- <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a> --}}
                                            @endif
                                        @endauth
                                    </div>
                                @endif
                            </li>
                            <li>
                                {{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm"> --}}
                                    {{-- <div class="container"> --}}
                                        {{-- <a class="navbar-brand" href="{{ url('/') }}">
                                            {{ config('app.name', 'Laravel') }}
                                        </a> --}}
                                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                            <!-- Left Side Of Navbar -->
                                            <ul class="navbar-nav me-auto">

                                            </ul>

                                            <!-- Right Side Of Navbar -->
                                            <ul class="navbar-nav ms-auto">
                                                <!-- Authentication Links -->
                                                @guest
                                                    @if (Route::has('login'))
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                                        </li>
                                                    @endif

                                                    @if (Route::has('register'))
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                                        </li>
                                                    @endif
                                                @else
                                                    <li class="nav-item dropdown">
                                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                            {{ Auth::user()->name }}
                                                        </a>

                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                            onclick="event.preventDefault();
                                                                            document.getElementById('logout-form').submit();">
                                                                {{ __('Logout') }}
                                                            </a>

                                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                                @csrf
                                                            </form>
                                                        </div>
                                                    </li>
                                                @endguest
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                            </li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="search_input" id="search_input_box">
			<div class="container">
				<form class="d-flex justify-content-between">
					<input type="text" class="form-control" id="search_input" placeholder="Search Here">
					<button type="submit" class="btn"></button>
					<span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
				</form>
			</div>
		</div>
	</header>
	<!-- End Header Area -->
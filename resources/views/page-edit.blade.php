{{-- @extends('layouts.app') --}}
@extends('index')

@section('content')


	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Edit Produk</h1>
					<nav class="d-flex align-items-center">
						<a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="#">Edit Produk</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Login Box Area =================-->
	<div class="edit">
		<br>
		<br>
		<form action="{{ url('update-produk', $edit->id) }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-grup">
				<p>
				<h5>Nama :</h5>
				<input type="text" id="nama" name="nama" placeholder="Nama Produk" value="{{ $edit->nama }}">                        
				</p>
			</div>
			<div class="form-grup">
				<br>
				<p>
				<h5>Kategori :</h5>
				<input type="text" id="kategori" name="kategori" class="kategori" placeholder="Kategori Produk" value="{{$edit->kategori}}}">
				</p>
			</div>
			<div class="form-grup">
				<br>
				<p>
				<h5>Harga :</h5>
				<input type="text" id="harga" name="harga" class="harga" placeholder="Harga Produk" value="{{ $edit->harga }}">
				</p>
			</div>
						<div class="form-grup">
				<br>
				<p>
				<h5>stok :</h5>
				<input type="text" id="stok" name="stok" class="stok" placeholder="Stok Produk" value="{{ $edit->stok }}">
				</p>
			</div>
			<div class="form-grup">
				<br>
				<p>
				<h5>Deskripsi :</h5>
				<input type="text" id="deskripsi" name="deskripsi" class="deskripsi" placeholder="Deskripsi Produk" value="{{ $edit->deskripsi }}">
				</p>
			</div>
			<div class="form-grup">
				<br>
				<label for="gambar1">Upload Gambar Produk :</label>
				<input type="file" class="form-control-file" id="gambar" name="gambar">
			</div>
			<div class="form-grup">
					<br>
					{{-- <button type="submit" class="button-succes">Buat</button> --}}
					<button type="submit" class="btn btn-primary">Jual Produk</button>
			</div>
		</form>
	</div>
<!--================End Login Box Area =================-->
	<br>
	<br>
@endsection

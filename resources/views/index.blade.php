<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>UIM STORE</title>
	<!--
		CSS
		============================================= -->
	{{-- <link rel="stylesheet" href="css/linearicons.css"> --}}
    <link href="{{ asset('css/linearicons.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/font-awesome.min.css"> --}}
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/themify-icons.css"> --}}
    <link href="{{ asset('css/themify-icons.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/bootstrap.css"> --}}
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/owl.carousel.css"> --}}
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/nice-select.css"> --}}
    <link href="{{ asset('css/nice-select.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/nouislider.min.css"> --}}
    <link href="{{ asset('css/nouislider.min.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/ion.rangeSlider.css" /> --}}
    <link href="{{ asset('css/ion.rangeSlider.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" /> --}}
    <link href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/magnific-popup.css"> --}}
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet"> 
	{{-- <link rel="stylesheet" href="css/main.css"> --}}
    <link href="{{ asset('css/main.css') }}" rel="stylesheet"> 
</head>

<body>

@include('header')
@yield('content')
@include('footer')

	{{-- <script src="js/vendor/jquery-2.2.4.min.js"></script> --}}
	<script src="{{ asset('js/vendor/jquery-2.2.4.min.js') }}"></script>	
	{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	crossorigin="anonymous"></script> --}}
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js') }}"></script>
	{{-- <script src="js/vendor/bootstrap.min.js"></script> --}}
	<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
	{{-- <script src="js/jquery.ajaxchimp.min.js"></script> --}}
	<script src="{{ asset('js/jquery.ajaxchimp.min.js') }}"></script>
	{{-- <script src="js/jquery.nice-select.min.js"></script> --}}
	<script src="{{ asset('js/jquery.nice-select.min.js') }}"></script>
	{{-- <script src="js/jquery.sticky.js"></script> --}}
	<script src="{{ asset('js/jquery.sticky.js') }}"></script>
	{{-- <script src="js/nouislider.min.js"></script> --}}
	<script src="{{ asset('js/nouislider.min.js') }}"></script>
	{{-- <script src="js/countdown.js"></script> --}}
	<script src="{{ asset('js/countdown.js') }}"></script>
	{{-- <script src="js/jquery.magnific-popup.min.js"></script> --}}
	<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
	{{-- <script src="js/owl.carousel.min.js"></script> --}}
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<!--gmaps Js-->
	{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script> --}}
	<script src="{{ asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE') }}"></script>
	{{-- <script src="js/gmaps.min.js"></script> --}}
	<script src="{{ asset('js/gmaps.min.js') }}"></script>
	{{-- <script src="js/main.js"></script> --}}
	<script src="{{ asset('js/main.js') }}"></script>
</body>

</html>
<?php

use App\Http\Controllers\WebControllerController;
use App\Models\WebController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page-index');
});

Route::get('produk',[WebControllerController::class, 'index']);
Route::get('detail/{id}', [WebControllerController::class, 'show']);
Route::get('jual-produk', [WebControllerController::class, 'create']);
Route::post('simpan-produk', [WebControllerController::class, 'store']);
Route::get('edit-produk/{id}', [WebControllerController::class, 'edit']);
Route::post('update-produk/{id}', [WebControllerController::class, 'update']);
Route::get('hapus/{id}', [WebControllerController::class, 'destroy']);


// Route::get('/jual-produk', function () {
//     return view('page-jual');
// });
Route::get('/dashboard', function () {
    return view('page-admin');
});
Route::get('/keranjang', function () {
    return view('page-keranjang');
});
// Route::get('/detail', function () {
//     return view('page-detail');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

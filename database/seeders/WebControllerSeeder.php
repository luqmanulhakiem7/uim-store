<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\WebController;

class WebControllerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WebController::create([
            'nama'=>'Nike Spesial Edition',
            'kategori'=>'Sepatu',
            'harga'=>'350.000',
            'stok'=>'1',
            'deskripsi'=>'sepatu limited edition',
            'gambar'=>'p3.jpg',
        ]);
        WebController::create([
            'nama'=>'Seragam TI Semester 3',
            'kategori'=>'Baju',
            'stok'=>'12',
            'deskripsi'=>'baju praktek teknik informatika semester 3',
            'harga'=>'110.000',
            'gambar'=>'p2.jpg',
        ]);
    }
}
